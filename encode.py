
import auro


def wbyte (file, n):
    file.write(bytes([n]))

def _wint (file, n):
    if (n > 127):
        _wint(file, n >> 7)
    wbyte(file, n & 127 | 128)

def wint (file, n):
    if (n > 127):
        _wint(file, n >> 7)
    wbyte(file, n & 127)

def wstr (file, value):
    bs = value.encode()
    wint(file, len(bs))
    file.write(bs)

def encode (file, outfile):
    with open(outfile, 'wb') as out:
        out.write(b'Auro 0.6\0')

        wint(out, len(file.modules))
        for m in file.modules:
            if isinstance(m, auro.Import):
                wint(out, 1)
                wstr(out, m.name)
            elif isinstance(m, auro.Define):
                wint(out, 2)
                wint(out, len(m.items))
                for name, item in m.items.items():
                    if isinstance(item, auro.Module):
                        wint(out, 0)
                    elif isinstance(item, auro.Type):
                        wint(out, 1)
                    elif isinstance(item, auro.Function) or isinstance(item, auro.Const):
                        wint(out, 2)
                    else:
                        raise Exception("What is this %r" % item)
                    wint(out, item.index)
                    wstr(out, name)
            elif isinstance(m, auro.Use):
                wint(out, 3)
                wint(out, m.base.index)
                wstr(out, m.name)
            elif isinstance(m, auro.Build):
                wint(out, 4)
                wint(out, m.base.index)
                wint(out, m.arg.index)
            else:
                raise Exception("What is this %r" % item)


        wint(out, len(file.types))
        for t in file.types:
            # 0 means 'unknown module'
            # 1.. means the module with index n-1
            wint(out, t.mod.index + 1)
            wstr(out, t.name)


        wint(out, len(file.fns))
        for f in file.fns:
            # 0 means code
            # 1 means 'unknown module'
            # 2... means the module with index n-2
            wint(out, f.mod.index + 2)

            wint(out, len(f.ins))
            for t in f.ins:
                wint(out, t.index)

            wint(out, len(f.outs))
            for t in f.outs:
                wint(out, t.index)

            wstr(out, f.name)

        wint(out, len(file.consts))
        for c in file.consts:
            if isinstance(c, auro.Buffer):
                wint(out, 2)
                wint(out, len(c.bytes))
                out.write(c.bytes)
            elif isinstance(c, auro.Call):
                wint(out, c.fn.index + 16)
                for arg in c.args:
                    wint(out, arg.index)
            else:
                raise Exception("What is this %r" % item)

        # Module section, with 1 empty define module, plus function section with 1 empty function
        #out.write(bytes([1, 2,0,   0,   1, 1,0,0,   0,   1,0,   0]))

        # 5 left empty sections
        out.write(bytes([0,0,0,0]))

