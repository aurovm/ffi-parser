
import sys


# This is not required if you've installed pycparser into
# your site-packages/ with setup.py
sys.path.extend(['./pycparser'])

from pycparser import c_parser, c_ast, parse_file

import os

class Type:
    def from_node(node, state):
        typ = type(node)

        # A full type with modifiers
        if typ == c_ast.TypeDecl:
            # Ignore the type modifiers
            return Type.from_node(node.type, state)

        # Specifically an identifier
        elif typ == c_ast.IdentifierType:
            base = maybe_primitive(node)

            if base == None and len(node.names) == 1:
                try:
                    base = state.typedefs[node.names[0]]
                except KeyError:
                    pass

            if base != None:
                return base

        elif typ == c_ast.PtrDecl:
            return Type.pointer(node.type, state)

        elif typ == c_ast.Struct:
            # TODO: Catch error
            # TODO: Not allow fields
            try:
                return state.structs[node.name]
            except KeyError:
                raise Exception("unknown struct name '%s'" % node.name)

        # Fallback to Unknown Type
        return Unknown(node)

    def pointer(node, state):
        # Pointer declarations have special treatment depending on the type.
        # So, each kind of pointer is assigned a different Type.

        typ = type(node)
        if typ == c_ast.TypeDecl:
            inner = node.type
            if type(inner) == c_ast.IdentifierType:
                if inner.names == ['void']:
                    return VoidPointer()
        elif typ == c_ast.FuncDecl:
            return FunctionPointer(FunctionDecl.from_node(node, state))

        # Any non recognized special case will be treated as a normal pointer
        return Pointer(Type.from_node(node, state))

class VoidType:
    def __str__(self):
        return "<Void>"

Void = VoidType()

# Contains a string with the name of a sized primitive type, using rust's naming convention.
class Primitive(Type):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

# Special kind of primitives whose size is unknown before compilation. uses C primitive names.
class CPrimitive(Primitive):
    pass

class Typedef(Type):
    def __init__(self, base):
        self.base = base

class Unknown(Type):
    def __init__(self, base):
        self.base = base

    def __str__(self):
        node = self.base
        return "?%s(%s)" % (type(node).__name__, typestr(node))

class Struct(Type):
    def __init__(self, fields = None):
        self.fields = fields

    def __str__(self):
        if self.fields != None:
            fields = ','.join(map(str, self.fields))
            return "struct {%s}" % fields
        else:
            return "struct"

class Pointer(Type):
    def __init__(self, base):
        self.base = base

    def __str__(self):
        return '*%s' % self.base

# The treatment of void pointers is different than normal pointers.
class VoidPointer(Type):
    def __str__(self):
        return '*void'

# TODO: Remove repetition with Function
class FunctionPointer(Type):
    def __init__(self, decl):
        self.decl = decl

    def __str__(self):
        params = ','.join([str(param) for param in self.decl.params])
        return '(%s)->%s' % (params, self.decl.ret)

# Actual function, not a function pointer type
class FunctionDecl:
    def __init__(self, params, ret):
        self.params = params
        self.ret = ret

    def from_node (node, state):
        def get_param_type (node):
            return Type.from_node(node.type, state)

        params = []
        if node.args:
            params = [get_param_type(param) for param in node.args]

        if params == [Void]:
            params = []

        ret = Type.from_node(node.type, state)
        if ret == Void:
            ret = None

        return FunctionDecl(params, ret)


c_types = set(["char", "int", "long", "float", "double"])

# Receives an IdentifierType and returns a primitive name if it is one
def maybe_primitive(node):
    name = ' '.join(node.names)

    if name == "void":
        return Void

    if node.names[-1] in c_types:
        return CPrimitive(name)

    # Machine types
    elif name == "_Bool":
        return Primitive("bool")
    elif name == "uint32_t":
        return Primitive("u32")


class State:
    def __init__(self):
        self.typedefs = {}
        self.structs = {}

        self.functions = {}

    def decl_struct(self, node):
        def type_from_field (node):
            if type(node) != c_ast.Decl:
                raise Exception("Field that is not a Declaration")
            return Type.from_node(node.type, self)

        s = Struct()

        # Either a declaration or a use
        if node.name:
            try:
                # If it's a use, replace the new struct with the previously declared one
                s = self.structs[node.name]

                # Cannot redefine structs
                if s.fields and node.decls:
                    raise Exception("redefinition of 'struct %s'" % node.name)

            except KeyError:
                # If the struct didn't exist, declare it
                self.structs[node.name] = s
                s.name = node.name

        if node.decls:
            s.fields = list(map(type_from_field, node.decls))

        return s

    def decl_ptr(self, node):
        typ = type(node)
        if typ == c_ast.TypeDecl:
            inner = node.type
            if type(inner) == c_ast.IdentifierType:
                if inner.names == ['void']:
                    return VoidPointer()
                else:
                    base = maybe_primitive(inner)
                    if base == None and len(inner.names) == 1:
                        base = self.typedefs[inner.names[0]]

                    if base != None:
                        return Pointer(base)
            # Print inner
            return Pointer(Unknown(inner))
        elif typ == c_ast.FuncDecl:
            return FunctionPointer(FunctionDecl.from_node(node, self))
        else:
            return Pointer(Unknown(node))

    def decl_type(self, node):
        typ = type(node)
        if typ == c_ast.IdentifierType:
            base = maybe_primitive(node)
            if base != None:
                return base
        elif typ == c_ast.Struct:
            return self.decl_struct(node)
        elif typ == c_ast.PtrDecl:
            # Pointer declarations have special treatment depending on the type
            return self.decl_ptr(node.type)

        # Any non handled case, just make it
        return Type.from_node(node, self)

    def decl_fn(self, node):
        #print("Declaring " + node.name)
        self.functions[node.name] = FunctionDecl.from_node(node.type, self)

    def add_decl(self, node):
        typ = type(node.type)

        if typ == c_ast.FuncDecl:
            self.decl_fn(node)
        else:
            if node.name == None:
                if typ == c_ast.Struct:
                    self.decl_struct(node.type)
                else:
                    # Unnamed type declaration.
                    # Ignore
                    return
            else:
                print('Variable declaration is not supported: %s' % node.name)
                return
        # TODO: Both of the ignored cases COULD be defining a type.
        # Handle that case.

    def add_type_def(self, node):
        ty = type(node.type)
        if ty == c_ast.TypeDecl:
            # Typedef and TypeDecl are the same thing. So, remove one level of redundancy.
            # In the end, the real type is in `node.type.type`
            if node.name != node.type.declname:
                raise Exception("Typedef whose name is not the same as it's TypeDecl")

            node = node.type
            self.typedefs[node.declname] = self.decl_type(node.type)
        elif ty == c_ast.PtrDecl:
            self.typedefs[node.name] = self.decl_type(node.type)
        else:
            raise Exception("Typedef %s which is not a TypeDecl nor PtrDecl: %s" % (node.name, typestr(node.type)))


    def add_node(self, node):
        typ = type(node)

        if typ == c_ast.Typedef:
            self.add_type_def(node)
        elif typ == c_ast.Decl:
            self.add_decl(node)
        elif typ == c_ast.FuncDef:
            print("Function definition not supported: %s" % node.decl.name)
        else:
            raise Exception("Unsupported Node Type: " + str(typ))


def nodestr(decl_node):
    """ Receives a c_ast.Decl note and returns its explanation in
        English.
    """
    return (typestr(decl_node.type) or '<%s>' % type(decl_node.type).__name__) + ' ' + str(decl_node.name)

def typestr(decl):
    """ Recursively explains a type decl node
    """
    typ = type(decl)

    if typ == c_ast.TypeDecl:
        return typestr(decl.type)
    elif typ == c_ast.Typename or typ == c_ast.Decl:
        return typestr(decl.type)
    elif typ == c_ast.IdentifierType:
        return ' '.join(decl.names)
    elif typ == c_ast.PtrDecl:
        return '*' + typestr(decl.type)
    elif typ == c_ast.ArrayDecl:
        arr = '[%s]' % decl.dim.value if decl.dim else '[]'

        return typestr(decl.type) + arr

    elif typ == c_ast.FuncDecl:
        if decl.args:
            params = [typestr(param) for param in decl.args.params]
            args = ','.join(params)
        else:
            args = ''

        return '%s(%s)' % (typestr(decl.type), args)

    elif typ == c_ast.Struct:
        if decl.decls:
            decls = [nodestr(mem_decl) for mem_decl in decl.decls]
            members = ','.join(decls)
            body = '{%s}' % members if members else ''
        else:
            body = ''

        return 'struct' + (' ' + decl.name if decl.name else '') + body

    elif typ == c_ast.Enum:
        values = [value.name for value in decl.values]
        members = ','.join(values)
        body = '{%s}' % members if members else ''

        return 'enum' + (' ' + decl.name if decl.name else '') + body
    else:
        return "<%s>" % typ.__name__


def parse_ast(filename):
    path = os.path.dirname(os.path.realpath(__file__))
    fake_libc = "-I%s/pycparser/utils/fake_libc_include" % path
    taboutput = "%s/taboutput" % path

    ast = parse_file(
        filename,
        use_cpp = True,
        cpp_args = fake_libc,
        parser = c_parser.CParser(
            taboutputdir = taboutput
        )
    )
    return ast

def parse (filename):
    ast = parse_ast(filename)
    state = State()
    for node in ast.ext:
        state.add_node(node)
    return state

if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename  = sys.argv[1]
    else:
        filename = 'pycparser/examples/c_files/memmgr.c'

    show_func_defs(filename, None)