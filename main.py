
import sys
import auro
import c
import encode

class TypeDesc:
    def __init__(self, ctp, tp=None, new=None, get=None):
        # c type
        self.ctp = ctp
        # auro friendly type
        self.tp = tp
        # Converts a type into a c type
        self.new = new
        # Converts a c type into a type
        self.get = get

class TypeMatch:
    def __init__(self, tp, names):
        self.tp = tp
        self.names = names

    def has_name (self, name):
        return name in self.names


class Compiler:
    def __init__(self, filename, libname):
        self.filename = filename
        self.libname = libname
        self.parsed = c.parse(filename)

        # Raw signatures directly matching the C functions
        self.main = auro.File()
        self.main.basics()

        self.primitives = {}
        self.auro_prim_types = [
            TypeMatch(self.main.int, ["cint", "clong"])
        ]


    def basics (self):
        base = self.main.add(auro.Use(self.main.ffi, "import"))

        name = self.main.str_const(self.libname)
        args = self.main.add(auro.Define())
        args.items["0"] = name

        self.cmod = self.main.add(auro.Build(base, args))
        self.cmod_get = self.main.add(auro.Use(self.cmod, "get"))

    def get_prim (self, name, mod, modkey):
        if not name in self.primitives:
            ctp = self.main.type(mod, modkey)

            tp = None
            if name == "int":
                tp = self.main.int

            self.primitives[name] = TypeDesc(ctp)
        return self.primitives[name]

    def compile_type (self, node):
        file = self.main

        # Cached
        if hasattr(node, '__auro__'):
            return node.__auro__

        tp = None

        # Must first than c.Primitive because it's more general
        if isinstance(node, c.CPrimitive):
            tp = self.get_prim("c" + node.name, self.main.cffi, node.name)
        elif isinstance(node, c.Primitive):
            tp = self.get_prim(node.name, self.main.prim, node.name)
        elif isinstance(node, c.Pointer):
            tp = TypeDesc(self.main.ptr)
        elif isinstance(node, c.FunctionPointer):
            tp = TypeDesc(self.main.ptr)
        elif isinstance(node, c.VoidPointer):
            tp = TypeDesc(self.main.ptr)
        else:
            print("Unknown type %r" % node)
            tp = None

        if tp != None:
            node.__auro__ = tp
        return tp


    def compile_function (self, name, node):

        params = [self.compile_type(p) for p in node.params]

        has_native = True

        for param in params:
            if param == None:
                return
            if param.tp == None:
                has_native = False

        ret = None
        if node.ret != None:
            ret = self.compile_type(node.ret)
            if ret == None:
                return
            if ret.tp == None:
                has_native = False

        #print('supported function:', name)

        args_mod = self.main.add(auro.Define())
        args_mod.items["name"] = self.main.str_const(name)

        if ret != None:
            args_mod.items["out"] = ret.ctp

        for i in range(0, len(params)):
            args_mod.items["in%d" % i] = params[i].ctp

        fmod = self.main.add(auro.Build(self.cmod_get, args_mod))
        cfn = self.main.fn(fmod, '', [p.ctp for p in params], [ret.ctp] if ret != None else [])

        # The raw form of the function has a raw suffix in it's name
        self.main.export.items[name] = cfn

        # When we do native
        #self.main.export.items[name + "\x1fraw"] = cfn

        if has_native:
            pass
        else:
            pass
            #print("Can't create a native form for function %s" % name)


        return True


if __name__ == "__main__":
    if len(sys.argv) == 4:
        in_file  = sys.argv[1]
        out_file  = sys.argv[2]
        libname  = sys.argv[3]
    else:
        print("Usage: %s <header> <out-file> <object-file>" % sys.argv[0])
        sys.exit(1)

    compiler = Compiler(in_file, libname)
    compiler.basics()

    #for name, tp in compiler.parsed.types.items():
        # Assign a name, so that it can be exported by auro if it's complex
        #tp.name = name

    for name, function in compiler.parsed.functions.items():
        success = compiler.compile_function(name, function)
        if not success:
            print('Function %s can\'t be compiled' % name)

    compiler.main.recompute_indices()
    encode.encode(compiler.main, out_file)

    #print("Types: %s" % list(compiler.parsed.typedefs.keys()))
    #print("Functions: %s" % list(compiler.parsed.functions.keys()))