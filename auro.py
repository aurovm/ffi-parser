
# To understand what these types mean, check the auro format Spec.
# These correspond mostly one to one to the items defined there.

class Item:
    pass

class Module(Item):
    pass

class Define(Module):
    """Define Module"""
    def __init__(self):
        self.items = {}

class Import(Module):
    """Imported Module"""
    def __init__(self, name):
        self.name = name

class Use(Module):
    """USe Module"""
    def __init__(self, base, name):
        self.base = base
        self.name = name

class Build(Module):
    """Build Module"""
    def __init__(self, base, arg):
        self.base = base
        self.arg = arg


class Type(Item):
    def __init__(self, mod, name):
        self.mod = mod
        self.name = name

class Function(Item):
    """Imported function. FFI modules don't have code functions, only imports from modules"""
    def __init__(self, mod, name, ins, outs):
        self.mod = mod
        self.name = name
        self.ins = ins
        self.outs = outs


class Const(Item):
    pass

class Buffer(Const):
    def __init__(self, bytes):
        self.bytes = bytes

class Call(Const):
    """Call constant"""
    def __init__(self, fn, args):
        self.fn = fn
        self.args = args



class File:
    def __init__(self):
        self.modules = []
        self.types = []
        self.fns = []
        self.consts = []

    def add (self, item):
        if   isinstance(item, Module):
            col = self.modules
        elif isinstance(item, Type):
            col = self.types
        elif isinstance(item, Function):
            col = self.fns
        elif isinstance(item, Const):
            col = self.consts
        else:
            raise Exception("Invalid item type: %s" % type(item))

        item.index = len(col)
        col.append(item)

        return item

    """
        Goes over all modules and constants and recomputes their indices,
        because for it's not as straightforward as just their index in the collections.

        This needs to be done before encoding.
    """
    def recompute_indices(self):
        for m in self.modules:
            m.index += 1

        for c in self.consts:
            c.index += len(self.fns)

    # Utility item definition functions
    def _import(self, name):
        return self.add(Import(name.replace(".", "\x1f")))

    def use(self, mod, name):
        return self.add(Use(mod, name))

    def type(self, mod, name):
        return self.add(Type(mod, name))

    def fn(self, mod, name, ins, outs):
        return self.add(Function(mod, name, ins, outs))



    def str_const(self, value):
        buf = self.add(Buffer(str.encode(value)))
        val = self.add(Call(self.str_new, [buf]))
        return val

    def basics (self):
        # By not being in self.modules, it's index won't be recomputed. It will stay 0
        self.argument = Module()
        self.argument.index = 0

        # First module in self.modules. It's index is 0 but when recomputed, it will be 1
        self.export = self.add(Define())

        # Mods
        self.ffi = self._import("auro.ffi")
        self.cffi = self._import("auro.ffi.c")
        self.mem = self._import("auro.machine.mem")
        self.prim = self._import("auro.machine.primitive")
        self.buf_mod = self._import("auro.buffer")
        self.int_mod = self._import("auro.int")
        self.str_mod = self._import("auro.string")

        # Types
        self.int = self.type(self.int_mod, "int")
        self.ptr = self.type(self.mem, "pointer")
        self.buffer = self.type(self.buf_mod, "buffer")
        self.string = self.type(self.str_mod, "string")

        # Functions
        self.str_new = self.fn(self.str_mod, "new", [self.buffer], [self.string])
